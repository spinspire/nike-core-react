import React, { Component } from 'react';

const Loading = (props) => {
  return <img style={{margin: '0 auto'}}src="/assets/img/spinner-infinite.gif"/>
}

export default Loading;
