import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import PrimaryMenu from './PrimaryMenu';
import Footer from './Footer';
import Homepage from './Homepage';
import Node from './Node';
import WeeklySchedule from './pages/WeeklySchedule';
import SFL from './pages/SFL';
import WinningHabits from './pages/WinningHabits';
import UserInfo from './pages/UserInfo';

const App = (props) => {
  return(
    <div>
      <PrimaryMenu/>
      <Route path='/' exact component={Homepage} />
      <Route path='/node/:nid' component={Node} />
      <Route path='/weekly-view' exact component={WeeklySchedule} />
      <Route path='/sfl' exact component={SFL} />
      <Route path='/winning-habits' exact component={WinningHabits} />
      <Route path='/user/:uid/info' component={UserInfo} />
      <Footer/>
    </div>
 )
}
export default App;
