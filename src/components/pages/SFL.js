import React, { Component } from 'react';
import Accordion from '../Accordion';
import Loading from '../Loading';

export default class WeeklySchedule extends Component {
  constructor(props){
    super(props)
    this.state = {
      rows: []
    };
  }

  componentDidMount(){
    const {rows} = this.state;
    fetch('http://rest-server.christian.server1.spinspire.com/api/view/sfl',{
      headers: {}
    }).then(response => {
      return response.json();
    }).then(results => {
      let queue = '';
      let i = -1;
      results.data.map(result => {
        if(queue !== result.header){
          rows.push([])
          queue = result.header;
          i++;
        }
        // Create css safe (machine_name) class name which we'll use later for accordion code
        result.header_machine_name = result.header.replace(/\s+/g, '-').toLowerCase();
        rows[i].push(result);
      });
      this.setState({
        rows
      })
    })
  }

  render(){
    const {rows} = this.state;
    if(rows.length > 0) {
      return (<Accordion rows={rows} />)
    } else {
      return(
        <Loading />
      )
    }
  }
}
