import React, { Component } from 'react';
import Loading from './Loading';
import Course from './content_types/Course';
import BasicPage from './content_types/BasicPage';

export default class Node extends Component {
  constructor(props) {
    super(props)
    this.state = {
      nodeData: {}
    };
  }
  componentDidMount(){
    const {cardData} = this.state;
    const {nid} = this.props.match.params;
    if(nid) {
      fetch(`http://rest-server.christian.server1.spinspire.com/api/node/${nid}/type`,{
        headers: {}
      }).then(response => {
        return response.json();
      }).then(results => {
        this.setState({
          nodeData: {nid: nid, type: results.data[0].type}
        })
      })
    }
  }
  render() {
    const {nodeData} = this.state;
    if(Object.keys(nodeData).length > 0) {
      switch(nodeData.type) {
        case 'course':
          return <Course data={nodeData} />
        case 'basic_page':
          return <BasicPage data={nodeData} />
        default:
          console.log(`No component for ${nodeData.type} has been created yet.`)
          return <h1>Oops looks like someething went wrong.</h1>;
      }
    } else {
      return (<Loading/>)
    }
  }
}
