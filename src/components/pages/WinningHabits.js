import React, { Component } from 'react';
import Accordion from '../Accordion';
import Loading from '../Loading';

export default class WinningHabits extends Component {
  constructor(props){
    super(props)
    this.state = {
      rows: []
    };
  }

  componentDidMount(){
    const {rows} = this.state;
    fetch('http://rest-server.christian.server1.spinspire.com/api/view/winning-habits',{
      headers: {}
    }).then(response => {
      return response.json();
    }).then(results => {
      let habit = '';
      let i = -1;
      results.data.map(result => {
        if(habit !== result.header){
          rows.push([])
          habit = result.header;
          i++;
        }
        // Create css safe (machine_name) class name which we'll use later for accordion code
        result.header_machine_name = result.header.replace(/\s+/g, '-').toLowerCase();
        rows[i].push(result);
      });
      this.setState({
        rows
      })
    })
  }

  render(){
    const {rows} = this.state;
    if(rows.length > 0) {
      return (<Accordion rows={rows} />)
    } else {
      return(
        <Loading />
      )
    }
  }
}
