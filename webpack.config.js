var webpack = require('webpack');
var path = require('path');

module.exports = {
  devtool: 'source-map',
  entry: {
    app: ['./src']
  },
  output: {
    path: path.join(__dirname, 'dest'),
    filename: 'bundle.js',
    sourceMapFilename: 'bundle.js.map'
  },
  devServer : {
    contentBase: path.resolve(__dirname, './src'),
  },
  resolve: {
    modules: ['node_modules', 'src'],
    extensions: ['.jsx','.js']
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': JSON.stringify('dev')
      }
    })
  ],
  module : {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: ['env','react']
            }
          }
        ],
      },
    ],
  },
};
