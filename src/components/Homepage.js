import React, { Component } from 'react';
import CourseCard from './CourseCard';

export default class Homepage extends Component {

  constructor(props){
    super(props);
    this.state = {
      cardData: []
    };
  }

  componentDidMount(){
    const {cardData} = this.state;
    fetch('http://rest-server.christian.server1.spinspire.com/api/homepage',{
      headers: {}
    }).then(response => {
      return response.json();
    }).then(results => {
      results.data.map((result,index)=> {
        const key = Math.floor(Math.random() * Math.floor(99999));
        cardData.push(<CourseCard key={key} img={`../../assets/img/styles/300x235/${result.image}`} title={result.title} subtitle={result.body} url={'/node/'+result.nid} type={result.type}/>);
      })
      this.setState({
        cardData: cardData
      })
    })
  }
  render() {
    return(
      <div className="ncss-container fixed-fluid mt6-sm mb6-sm mt8-lg mb8-lg pb4-sm pb6-lg">
        <div className="hero-wrapper">
          <div className="pathway-landing-page hero-banner clearfix">
          </div>
        </div>
        <section>{this.state.cardData}</section>
      </div>
    )
  }
}
