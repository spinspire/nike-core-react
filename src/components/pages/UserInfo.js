import React, { Component } from 'react';
import Loading from '../Loading';

export default class WinningHabits extends Component {
  constructor(props){
    super(props)
    this.state = {
      fields: []
    };
  }

  componentDidMount(){
    const {fields} = this.state;
    const {uid} = this.props.match.params;
    fetch(`http://rest-server.christian.server1.spinspire.com/api/user/${uid}/info`,{
      headers: {}
    }).then(response => {
      return response.json();
    }).then(results => {
      this.setState({
        fields: results.data[0]
      })
    })
  }

  render(){
    const {fields} = this.state;
    if(Object.keys(fields).length > 0) {
      return(
        <div className="ncss-container fixed-fluid mt6-sm mb6-sm mt8-lg mb8-lg pb4-sm pb6-lg">
          <p>Name: {fields.firstname} {fields.lastname}</p>
          <p>Pathway: {fields.term_name}</p>
        </div>)
    } else {
      return(
        <Loading />
      )
    }
  }
}
