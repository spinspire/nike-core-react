(function ($) {

  // ======= Define and comment reusable function here ========

  $(document).ready(function () {

    // Allow active states to work
    document.addEventListener("touchstart", function () {}, true);
    // Check to see if we should support touch
    var supportsTouch = (typeof (window.ontouchstart) != 'undefined');
    var body = document.getElementsByClassName('body')[0];

    // ======= Global nav ========
    // Desktop Search
	// Stretch search field on focus
	$(".js-search-input").bind('focus blur', function(){
		$(this).parents('.js-global-search-container').toggleClass('focus');
	});

	$(".js-search-input").on('input', function(){
		if ($(this).val() === "") {
			//console.log("empty input");
			$(".js-btn-clear-input").addClass("u-sm-h");
		} else {
			//console.log("has value");
			$(".js-btn-clear-input").removeClass("u-sm-h");
		}
	});
	// Clear search field functionality
	$(".js-btn-clear-input").click(function(){
		$(".js-search-input").val("");
		$(this).addClass("u-sm-h"); // self destruct
	})


// Desktop States
	// Log in / Log out switch
	$(".js-login-btn").click(function(){
		var body = $("body");
		if (!body.hasClass("logged-in")) {
			console.log("log me in")
			body.addClass("logged-in");
			$(".js-my-account").removeClass("d-sm-h");
			$(".js-login-cta").addClass("d-sm-h");
		} else {
			console.log("log me out")
			body.removeClass("logged-in");
			$(".js-my-account").addClass("d-sm-h");
			$(".js-login-cta").removeClass("d-sm-h");
		}
	});

// Mobile Nav
	// Open & Close Functionality
	$(".js-open-nav").click(function(e){
		e.preventDefault();
		$(".offcanvas-nav").addClass("display");
		$(".overlay").addClass("show");
		setTimeout(function(){
			$(".offcanvas-nav").addClass("slide-in");
		}, 10)

	})
	$(".js-close-nav, .overlay").click(function(e){
		e.preventDefault();
		$(".offcanvas-nav, .js-submenu-panel").removeClass("slide-in");
		$(".overlay").removeClass("show");
		setTimeout(function(){
			$(".offcanvas-nav, .js-submenu-panel").removeClass("display")
		}, 300)
	})
	// Submenu Functionality
	$(".js-has-submenu").click(function(e){
		var targetMenu = $(this).data("menu");

		$(targetMenu).addClass("display");
		setTimeout(function(){
			$(targetMenu).addClass("slide-in");
		}, 10)

	})
	$(".js-btn-back").click(function(e){
		var thisSubmenu = $(this).parentsUntil($("li"), ".js-submenu-panel");
		var thisMenu = $(this).parent(".js-nav-panel");

		thisMenu.removeClass("slide-in");
		setTimeout(function(){
			thisMenu.removeClass("display")
		}, 300)
	})


// Mobile Search
	// Modal Functionality
	$(".js-trigger-search-modal").click(function(e){
		$(".js-modal-search-sm").addClass("display");
		setTimeout(function(){
			$(".js-modal-search-sm").addClass("show");
		}, 10)
		$(".js-mobile-search-input").focus();
	})
	$(".js-btn-close-search").click(function(e){
		$(".js-modal-search-sm").removeClass("show");
		setTimeout(function(){
			$(".js-modal-search-sm").removeClass("display");
		}, 10)
	})

  // ======= Accordions  =========================================================
    setTimeout(function() {

    var currentSection;
    var previousSection;

    // Toggle active sections
    function onSectionClick(section) {
      previousSection = currentSection;
      currentSection = section;

      // If new section is chosen close the currently opened one
      if (previousSection && previousSection.find('.accordion-header-component').attr('data-header') != currentSection.find('.accordion-header-component').attr('data-header')) {
        closeSection(previousSection);
      }

      if (currentSection.hasClass('active-section')) {
        closeSection(currentSection);
      } else {
        openSection(currentSection);
      }
    }

    // animate opening of section
    function openSection(current) {
      current.addClass('active-section');
      current.find('.js-section-content').slideDown();
      current.find('.accordion-header-component .open-close').removeClass('g72-arrow-thick-down').addClass('g72-arrow-thick-up');
    }

    // animate closing of a section
    function closeSection(previous) {
      previous.removeClass('active-section');
      previous.find('.js-section-content').slideUp();
      previous.find('.accordion-header-component .open-close').removeClass('g72-arrow-thick-up').addClass('g72-arrow-thick-down');
    }

    $('.accordion-header-component').click(function (e) {
      if($(e.currentTarget).prop('tagName') == 'HEADER') {
        var current = $(e.currentTarget).parent();
      }
      onSectionClick(current);
    });
  }, 150);


  setTimeout(function() {
    var body = $('.ncss-container section .field.field-body');
    // A lookup table of jQuery selectors as keys and CSS classes as values to be applied to the body HTML
    // Any new requirements should be added to the following lookup table (key=selector, value=classes)
    var selector_classes = {
        'h1': 'ncss-brand text-color-black fs30-sm fs40-lg u-uppercase',
        'h2': 'ncss-brand text-color-black fs24-sm fs30-lg u-uppercase',
        'h3': 'ncss-brand text-color-black fs18-sm fs22-lg pt8-sm pb3-sm u-uppercase',
        'h4': 'ncss-brand text-color-black fs16-sm fs18-lg pt8-sm pb3-sm u-uppercase',
        'p': 'text-color-grey pb4-sm',
        'ul': 'ncss-list-ul pl3-lg pl3-sm pb3-sm',
        'ol': 'ncss-list-ol pl3-lg pl3-sm pb3-sm',
        'ul>li, ol>li': 'ncss-li',
    };
    // iterate over the lookup table, query by selector, and add the corresponding classes to that selector
    for (var sel in selector_classes) {
        var classes = selector_classes[sel];
        // query and add classes
        body.find(sel).addClass(classes);
    }
    var unblockBtn = $('#entry-unlock').on('click',function(){
      $('.log-fields').removeClass('d-sm-h');
    });
  },100);
  });

})(jQuery);
