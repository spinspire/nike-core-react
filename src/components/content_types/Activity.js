import React, { Component } from 'react';

export default class Activity extends Component {
  constructor(props){
    super(props)
    this.state = {
      fields: {}
    };
  }
  componentDidMount(){
    const {nid} = this.props;
    if(nid) {
      fetch(`http://rest-server.christian.server1.spinspire.com/api/node/activity/${nid}`,{
        headers: {}
      }).then(response => {
        return response.json();
      }).then(results => {
        this.setState({
          fields: results.data[0]
        })
      })
    }
  }
  render() {
    const {fields} = this.state;
    return(
      <div className="d-sm-flx">
        <section className="ncss-col-sm-6 ncss-col-lg-4 ncss-col-lg-offset-1 pt8-sm">
          <span className="fs24-sm">Activity</span>
          <div className="field field-body">
            <p className="text-color-grey" dangerouslySetInnerHTML={{__html: fields.body}}/>
          </div>
        </section>
        <section className="ncss-col-sm-6 ncss-col-lg-4 ncss-col-lg-offset-1 pt8-sm">
          <div>
            <p className="pb4-sm">Once you've completed the Workout and Practice, click the button to unlock the Log Entry.</p>
            <a className="ncss-btn-black ncss-brand pt2-sm pb2-sm prl4-sm prl6-md d-sm-ib u-uppercase" id="entry-unlock">Unlock Log Entry</a>
          </div>
          <div className="log-fields d-sm-h">
            <span className="fs24-sm">Log Entry</span>
            <div className="field log-title ncss-input-container" id="edit-title-field">
              <label className="ncss-label">Title</label>
              <input className="ncss-input text-full required" type="text" id="edit-title-field" size="60" maxLength="255"/>
            </div>
            <div className="field textarea log-response ncss-input-container">
              <label className="ncss-label">Your Response</label>
              <textarea className="ncss-input text-full required" id="edit-body-field" cols="60" rows="20"/>
            </div>
            <div className="form-item field file-upload">
              <label htmlFor="field-image-upload ncss-label">Upload a Photo </label>
              <div className="image-widget clearfix">
                <div className="image-widget-data ncss-input-container">
                  <input type="file" id="edit-field-image-upload ncss-input" name="files[field_image_und_0]" size="22"/>
                  <a href="#" style={{display: 'none'}}>Clear Selection</a>
                  <input type="submit" id="edit-field-image-upload-button" className="ncss-btn-black ncss-brand u-uppercase prl6-sm pt2-sm pb2-sm d-sm-b" name="field_image_upload_button" value="Save"/>
                </div>
              </div>
            </div>
          </div>
          </section>
        </div>
      )
    }
}
