import React, { Component } from 'react';
import Loading from '../Loading';

export default class BasicPage extends Component {
  constructor(props){
    super(props)
    this.state = {
      fields: []
    };
  }
  componentDidMount(){
    const {nid,type} = this.props.data;
    if(nid) {
      fetch(`http://rest-server.christian.server1.spinspire.com/api/node/${type}/${nid}`,{
        headers: {}
      }).then(response => {
        return response.json();
      }).then(results => {
        this.setState({
          fields: results.data[0]
        })
      })
    }
  }

  render(){
    const {fields} = this.state;
    if(Object.keys(fields).length > 0) {
      return(
        <div className="ncss-container fixed-fluid mt6-sm mb6-sm mt8-lg mb8-lg pb4-sm pb6-lg">
          <section>
            <div className="field field-title ncss-brand">
              <h1 className="fs24-sm mb2-sm u-bold mt4-sm mb2-sm">{fields.title}</h1>
            </div>
            <div className="field field-body" dangerouslySetInnerHTML={{__html: fields.body}}></div>
          </section>
      </div>
      )
    } else {
      return <Loading/>
    }
  }
}
