import React, { Component } from 'react';

const Accordion = (props) => {
  const {rows} = props;
  return(
    <div className="accordion-layout ncss-container fixed-fluid">
    {
      rows.map(row =>
        <section className={`js-${row[0].header_machine_name} section-layout border-top-light-grey mr0-lg ml0-lg mr2-sm ml2-sm`}>
          <header className="accordion-header-component ncss-row mr0-sm ml0-sm pt7-sm pb7-sm prl2-sm" data-header={row[0].header_machine_name}>
            <div className="ncss-col-sm-8">
              <h3 className="heading ncss-brand u-uppercase fs18-sm fs16-lg">{row[0].header}</h3>
            </div>
            <div className="ncss-col-sm-4 ta-sm-r">
              <div className="open-close g72-arrow-thick-down">
                <span className="ncss-brand">0 of {row.length}</span>
              </div>
            </div>
          </header>
          <div className="js-section-content section-content prl2-sm">
            {
              row.map(item =>
                <div className="d-lg-flx flx-dir-lg-r mt6-sm mb6-sm">
                  <div className="show-for-large-only ncss-col-lg-2">
                    <img src={`/assets/img/styles/220x172/${item.image}`} />
                  </div>
                  <div className="ncss-col-sm-12 ncss-col-lg-8 ncss-col-lg-offset-1">
                    <h2 className="ncss-brand fs24-sm fs30-lg u-uppercase d-sm-ib">
                      <a href={`/node/${item.nid}`}>{item.title}</a>
                    </h2>
                    <p className="text-color-gray" dangerouslySetInnerHTML={{ __html: item.body}} />
                  </div>
                </div>
              )
            }
          </div>
        </section>
      )
    }
    </div>
  )
}
export default Accordion;
