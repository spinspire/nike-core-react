import React, { Component } from 'react';
import Activity from './Activity';
import Loading from '../Loading'

export default class Course extends Component {
  constructor(props) {
    super(props)
    this.state = {
      fields: {}
    };
  }
  componentDidMount(){
    const {fields} = this.state;
    const {nid,type} = this.props.data;
    if(nid) {
      fetch(`http://rest-server.christian.server1.spinspire.com/api/node/${type}/${nid}`,{
        headers: {}
      }).then(response => {
        return response.json();
      }).then(results => {
        this.setState({
          fields: results.data[0]
        })
      })
    }
  }
  render() {
    const {fields} = this.state;
    if(Object.keys(fields).length > 0) {
      return(
        <div className="ncss-container fixed-fluid mt6-sm mb6-sm mt8-lg mb8-lg pb4-sm pb6-lg">
          <section className="course-details">
            <div className="d-sm-flx">
              <div className="ncss-col-sm-6 ncss-col-lg-4 ncss-col-lg-offset-1">
                <div className="field course-image">
                  <img src={`/assets/img/${fields.image}`}/>
                </div>
                <div className="field course-button ncss-btn-black ncss-brand u-uppercase">
                  <a href="#" className="pt2-sm pb2-sm prl4-sm prl6-md d-sm-ib">Start Workout</a>
                </div>
                <div className="field course-status">
                  <span className="status-icon course-unlocked"></span>
                  <p className="fs12-sm"><strong>Ready, set...</strong><br/> Click the "Start Workout" button to begin.</p>
                </div>
              </div>
              <div className="ncss-col-sm-6 ncss-col-lg-4 ncss-col-lg-offset-1">
                <div className="field field-title">
                  <h1 className="ncss-brand fs24-sm fs30-lg">{fields.title}</h1>
                </div>
                <div className="field field-body" dangerouslySetInnerHTML={{__html: fields.body}}></div>
                <div className="field field-winning-habit">
                  <div className="ncss-input-container">
                    <label className="ncss-label mt4-sm">Winning Habit:</label>
                    <a className="text-color-grey u-underline" href="/winning-habit/field-best-team">Field the best team</a>
                  </div>
                </div>
                <div className="field field-resources">
                  <div className="ncss-input-container">
                    <label className="ncss-label mt4-sm">Resources:</label>
                    {fields.resources.map(resource => {
                      const key = Math.floor(Math.random() * Math.floor(99999));
                      return (<p><a className="text-color-grey u-underline" key={key} href={resource.url}>{resource.title}</a></p>)
                    })}
                  </div>
                </div>
              </div>
            </div>
          </section>
          <section className="activity-details">
            <Activity nid={fields.ref_node_nid} />
          </section>
        </div>
      )
    } else {
      return (<Loading />)
    }
  }
}
