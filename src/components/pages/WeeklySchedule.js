import React, { Component } from 'react';
import Accordion from '../Accordion';

export default class WeeklySchedule extends Component {
  constructor(props){
    super(props)
    this.state = {
      rows: []
    };
  }
  componentDidMount() {
    const {rows} = this.state;
    fetch('http://rest-server.christian.server1.spinspire.com/api/view/weekly-view',{
      headers: {}
    }).then(response => {
      return response.json();
    }).then(results => {
      let week = 0;
      results.data.map(result => {
        if(week !== result.week){
          rows.push([])
          week = result.week;
        }
        // Create css safe (machine_name) class name which we'll use later for accordion code
        result.header_machine_name = result.header.toString().replace(/\s+/g, '-').toLowerCase();
        result.header = 'week ' + result.header;
        rows[week-1].push(result);
      });
      this.setState({
        rows
      })
    })
  }

  render(){
    const {rows} = this.state;
    if(rows.length > 0) {
      return (<Accordion rows={rows} />)
    } else {
      return(
        <div>Weekly Schedule</div>
      )
    }
  }
}
