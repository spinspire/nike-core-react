import React, { Component } from 'react';

const CourseCard = (props) => {
  return(
    <div className={`ncss-col-sm-12 ncss-col-md-6 ncss-col-lg-3 va-sm-t mb12-sm prl4-md course-card node-${props.type}`}>
      <div className="field-course-image">
        <a href={props.url}><img src={props.img}/></a>
      </div>
      <div className="ncss-brand fs16-sm fs18-md fs20-lg pt4-sm pb2-sm u-uppercase field-title">
        <h4 className="ncss-brand fs16-sm fs18-md fs20-lg pt4-sm pb2-sm u-uppercase">
          <a href={props.url} target="_top">{props.title}</a>
        </h4>
      </div>
      <div className="text-color-grey clearfix field-body">
        <article role="article" dangerouslySetInnerHTML={{__html: props.subtitle}} />
      </div>
    </div>
  )
}
export default CourseCard;
